<?php require_once('ndsApi.php') ?>

<!DOCTYPE html>
<html  <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.ico" type="image/x-icon" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?php global $page, $paged; ?>
            <?php wp_title('|', true, 'right'); ?>
            <?php bloginfo('name'); ?>
            <?php $site_description = get_bloginfo('description', 'display'); ?>
            <?php if ($site_description && ( is_home() || is_front_page() )) echo " | $site_description"; ?>
            <?php if ($paged >= 2 || $page >= 2) echo ' | ' . sprintf(__('Page %s', 'twentyeleven'), max($paged, $page)); ?>
        </title>

        <!-- <link href='<?php bloginfo('template_directory'); ?>/css/editor-style.css' rel='stylesheet' type='text/css'> -->
        <link href='<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
        <link href='<?php bloginfo('template_directory'); ?>/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
        <link href='<?php bloginfo('template_directory'); ?>/css/animate.css' rel='stylesheet' type='text/css'>
        <link href='<?php bloginfo('template_directory'); ?>/css/sweetalert.css' rel='stylesheet' type='text/css'>
        <link href='<?php bloginfo('template_directory'); ?>/css/colorbox.css' rel='stylesheet' type='text/css'>
        <link href='<?php bloginfo('template_directory'); ?>/css/nivo-slider.css' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />

        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.form.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/sweetalert.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.colorbox-min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.nivo.slider.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        <?php if (is_singular() && get_option('thread_comments')): ?>
            <?php wp_enqueue_script('comment-reply'); ?>
        <?php endif; ?>

        <?php wp_head(); ?>

    </head>
    <body <?php body_class(); ?>>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-1 col-xs-6">
                        <a href="<?php echo home_url(); ?>">
                            <img src="<?php bloginfo('template_directory'); ?>/img/header_logo.png" class="img-responsive logo_head">
                        </a>
                    </div>

                    <div class="col-xs-6 hidden-lg hidden-md hidden-sm text-right">
                        <?php $plain_menu = get_plain_menu(); ?>
                        <div class="dropdown" style="padding: 5px 0px 0px 5px;">
                            <button class="btn btn-default" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-bars"></i>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                <?php foreach ($plain_menu as $each_res_menu): ?>
                                    <li>
                                        <a href="<?php echo $each_res_menu['url']; ?>"><?php echo $each_res_menu['title']; ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-8 hidden-xs">
                        <?php wp_nav_menu(array('theme_location' => 'primary', 'container_class' => 'menu-main-menu-container bold clearfix thaisans', 'menu_class' => 'menu clearfix  pull-right')); ?>
                    </div>
                </div>
            </div>
        </section>