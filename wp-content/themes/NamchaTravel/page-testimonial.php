<?php get_header(); ?>
<section class="bg_mountain">
	<div class="container">
		<?php wp_reset_postdata(); ?>
		<div class="row">
			<div class="col-xs-12">
				<h1 class="green shadow text-center page-title"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-8">
				<?php $page = get_query_var('page') ?>
				<?php $page = $page ? $page : 1; ?>
				<?php $testimonials = get_testimonial($page, 10); ?>

				<div class="row">
					<?php foreach($testimonials['data'] as $k => $testimonial): ?>
						<div key="<?php echo $k; ?>" style="margin-bottom: 15px;">
							<div class="testimonial_info bg_green white">
								<?php echo $testimonial->detail; ?>
							</div>
							<div class="clearfix">
								<div class="pull-right">
									<img src="<?php bloginfo('template_directory'); ?>/img/testimonial_bubble.png" >
								</div>
								<div class="pull-right text-right" style="margin-right: 15px;">
									<h4 style="margin-bottom: 0px;"><?php echo $testimonial->name; ?></h4>
									<p><?php echo $testimonial->human_date; ?></p>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>

				<div class="row">
                    <div class="col-xs-12">
                        <nav aria-label="" class="text-center">
                            <ul class="pagination pagination-sm">
                                <?php for ($i = 1; $i <= $testimonials['total_page']; $i++): ?>
                                    <li <?php echo $page == $i ? "class='active'" : ""; ?>>
                                        <a href="/testimonial/?page=<?php echo $i; ?>"><?php echo $i; ?></a>
                                    </li>
                                <?php endfor; ?>
                            </ul>
                        </nav>
                    </div>
                </div>
			</div>
			<div class="col-sm-4">
                <?php if($GLOBALS['requestStatus'] === true): ?>
                    <div class='alert alert-success'>
                        <h4>Success</h4>
                        Thank you for your comment.
                    </div>
                <?php endif; ?>

                <?php if($GLOBALS['requestStatus'] === false): ?>
                    <div class='alert alert-danger'>
                        <h4>Error</h4>
                        System cannot process your request, please try again later.
                    </div>
                <?php endif; ?>

				<form class="form" method='post' id="testimonialForm">
					<label>Write your own !</label>
					<div class="form-group guestFirstName">
                        <input type="text" class="form-control" name="guest-name" placeholder="Name" required="required" />
                    </div>
					<div class="form-group guestLastName">
                        <input type="text" class="form-control" name="guest-lastname" placeholder="Last Name" required="required" />
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="guest-email" placeholder="Email" required="required" />
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Message" name="guest-detail" rows="7"></textarea>
                    </div>
                    <div class="form-group clearfix">
                        <div class='pull-right'>
                            <div class="g-recaptcha" data-sitekey="6LcLKB0UAAAAAEbvWjvAPqMzKsnhKsJ3IAEDm8DF"></div>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <button
                            type="submit"
                            class="btn btn-green btn-round">
                            Send
                        </button>
                        <button class="btn btn-link grey" type="reset">Clear all</button>
                    </div>
                    <input type="hidden" name="mode" value='testimonial'>
				</form>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>