<?php
if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Index Widget',
        'id'   => 'header_widget_1'
    ));
    register_sidebar(array(
        'name' => 'Other Widget',
        'id'   => 'header_widget_2'
    ));
}

function trycatch_setup()
{

    /* Register customize menu */
    register_nav_menu('primary', __('Primary Menu', 'trycatch_setup'));
    register_nav_menu('secondary', __('Secondary Menu', 'trycatch_setup'));

    /* Register featured image support */
    add_theme_support('post-thumbnails', array('post'));
}

/**
 * 
 * @param string $menu_name menu location name default is promary
 * @return array
 */
function get_plain_menu($menu_name = 'primary')
{
    $menu_list = array();
    if (( $locations = get_nav_menu_locations() ) && isset($locations[$menu_name])) {
        $menu = wp_get_nav_menu_object($locations[$menu_name]);

        $menu_items = wp_get_nav_menu_items($menu->term_id);

        foreach ((array) $menu_items as $key => $menu_item) {
            $title = $menu_item->title;
            $url   = $menu_item->url;
            array_push($menu_list, array('url' => $url, 'title' => $title));
        }
        return $menu_list;
    } else {
        return $menu_list;
    }
}

function get_all_size_image($img_id)
{
    $link           = array();
    $link['img_id'] = $img_id;

    $temp              = wp_get_attachment_image_src($img_id);
    $link['thumbnail'] = $temp[0];

    $temp           = wp_get_attachment_image_src($img_id, 'medium');
    $link['medium'] = $temp[0];

    $temp          = wp_get_attachment_image_src($img_id, 'large');
    $link['large'] = $temp[0];

    return $link;
}

function get_all_post_image($post_id)
{
    $image       = array();
    $attachments = get_children(array('post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' => 'image'));
    foreach ($attachments as $attachment_id => $attachment) {
        array_push($image, get_all_size_image($attachment_id));
    }
    return $image;
}

function get_all_image_from_category($cate_id)
{
    $temp = get_posts(array('category' => $cate_id, 'orderby' => 'post_date', 'order' => 'DESC', 'posts_per_page' => -1));
    $img  = array();
    foreach ($temp as $each_post) {

        $post_now = get_all_post_image($each_post->ID);

        foreach ($post_now as $pic_post) {
            array_push($img, $pic_post);
        }
    }

    return $img;
}

function set_cookie($name, $value)
{
    $expire = 30 * 86400; // 30 Days;
    return setcookie($name, $value, $expire);
}

function get_cookie($name)
{
    if (isset($_COOKIE[$name])) {
        return $_COOKIE[$name];
    }

    return NULL;
}

/**
 * array_get($array, $key, $default = null)
 * @param array $array
 * @param string $key
 * @param mixed $default optional
 * @return mixed
 */
function array_get($array, $key, $default = null)
{
    return isset($array[$key]) ? $array[$key] : $default;
}
add_theme_support('post-thumbnails');
add_action('after_setup_theme', 'trycatch_setup');
add_action('after_setup_theme', 'register_sidebar');
add_filter('single_template', create_function('$t', 'foreach( (array) get_the_category() as $cat ) { if ( file_exists(TEMPLATEPATH . "/single-{$cat->term_id}.php") ) return TEMPLATEPATH . "/single-{$cat->term_id}.php"; } return $t;'));
function add_query_vars_filter( $vars ){
  $vars[] = "page";
  return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );