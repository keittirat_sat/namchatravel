<?php get_header(); ?>
<?php wp_reset_postdata(); ?>
<section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="green shadow text-center page-title"><?php wp_title(null); ?></h1>
			</div>
		</div>
		<div class="listCategory">
			<div class='row'>
				<div class='col-sm-10 col-sm-offset-1'>
					<?php foreach($posts as $post): setup_postdata($post); ?>
						<div class="row">
							<div class="col-sm-3">
								<div class="thumbnail">
									<?php $thumb = get_all_size_image(get_post_thumbnail_id()); ?>
									<img src="<?php echo $thumb['thumbnail']; ?>" class="img-responsive">
								</div>
							</div>
							<div class="col-sm-9">
								<h4 class='green shadow'><?php the_title(); ?></h4>
								<?php the_excerpt(); ?>
								<p class='text-right'>
									<a href="<?php the_permalink(); ?>" class='btn btn-round btn-green'>
										Read more
									</a>
								</p>
							</div>
						</div>
						<?php wp_reset_postdata(); ?>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>