        <div class="top_overlay hidden-xs" style="margin-top: 30px;"></div>
        <section id="home_footer" class="bg_grey">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 text-right hidden-xs link_footer">
                        <a href="<?php home_url() ?>" style="padding: 0px;">
                            <img src="<?php bloginfo('template_directory'); ?>/img/footer_logo.png">
                        </a>
                        <a href="<?php echo $GLOBALS['fbLink']; ?>">
                            <img src="<?php bloginfo('template_directory'); ?>/img/footer_fblogo.png">
                        </a>
                        <a href="<?php echo $GLOBALS['instagramLink']; ?>">
                            <img src="<?php bloginfo('template_directory'); ?>/img/footer_iglogo.png">
                        </a>
                    </div>
                    <div class="col-sm-6 footer_address white">
                        <p><b>Address:</b> 188/684 soi 26 T.Sannameng A.Sansai Chiangmai 50210</p>
                        <p><b>Tel:</b> +66806770771, +66871785545</p>
                        <p><b>Email:</b> booking.namchatravel@gmail.com</p>
                    </div>
                </div>
            </div>
        </section>

        <?php wp_footer(); ?>

        <div id="fb-root"></div>
        <script type="text/javascript">
            $(function() {
                $('[rel=colorbox], .wp-block-gallery.colorbox a').colorbox({
                    maxWidth: '80%',
                    maxHeight: '80%'
                });

                $('input[name=contact-name]').removeAttr('required')
                $('input[name=contact-lastname]').attr('placeholder', 'Name');

                $('input[name=guest-name]').removeAttr('required')
                $('input[name=guest-lastname]').attr('placeholder', 'Name');
            });
        </script>
        <script>
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.9&appId=194531797284411";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56b38b1b0993b3da"></script>

        <!-- Google Tag Manager -->
        <script>
            (function(w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start': new Date().getTime(),
                    event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s),
                    dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-5T96757');
        </script>
        <!-- End Google Tag Manager -->

        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5T96757" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        </body>

        </html>