<script type="text/javascript">
    function contactSubmit() {
        $('#submitForm').submit();
    }
</script>
<section class="home_contact_section">
    <h1 class="text-center green shadow" style="margin: 30px 0px;">Contact</h1>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-5 col-sm-6">
                <div style="margin-bottom: 15px;">
                    <?php
                    /**
                     * Fix Api Key in plugin script
                     */
                    echo wp_gmaps_shortcode(array(
                        'api_key'    => 'AIzaSyA4UzDRbjkkxOl9Hzgden0RCDtUJCwLOVE',
                        // 'address'    => '188/684 soi 26 T.sannameng A. Sansai Chiangmai 50210',
                        'lat'        => 18.833635999999998,
                        'lng'        => 99.065747999999999,
                        'zoom'       => '14',
                        'height'     => '300px',
                        'width'      => '100%',
                        'marker'     => 1,
                        'infowindow' => false,
                    ));

                    ?>
                </div>
                <div class="home_address hidden-xs">
                    <p><b>Address:</b> 188/684 soi 26 T.Sannameng A.Sansai Chiangmai 50210</p>
                    <p><b>Tel:</b> +66806770771, +66871785545</p>
                    <p><b>Email:</b> booking.namchatravel@gmail.com</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-5">
                <?php if($GLOBALS['requestStatus'] === true): ?>
                    <div class='alert alert-success'>
                        <h4>Success</h4>
                        Email already sent, Please check your inbox we already sent a copy to yours.
                    </div>
                <?php endif; ?>

                <?php if($GLOBALS['requestStatus'] === false): ?>
                    <div class='alert alert-danger'>
                        <h4>Error</h4>
                        System cannot process your request, please contact us directly by email.
                    </div>
                <?php endif; ?>

                <form class="form" id='submitForm' method='post'>
                    <div class="form-group firstName">
                        <input type="text" class="form-control" name="contact-name" placeholder="Name" required="required" />
                    </div>
                    <div class="form-group lastName">
                        <input type="text" class="form-control" name="contact-lastname" placeholder="Lastname" required="required" />
                    </div>
                    <div class="form-group email">
                        <input type="email" class="form-control" name="contact-email" placeholder="Email" required="required" />
                    </div>
                    <div class="form-group subject">
                        <input type="text" class="form-control" name="contact-subject" placeholder="Subject" required="required" />
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Message" name="contact-detail" rows="7"></textarea>
                    </div>
                    <div class="form-group clearfix">
                        <div class='pull-right'>
                            <div class="g-recaptcha" data-sitekey="6LcLKB0UAAAAAEbvWjvAPqMzKsnhKsJ3IAEDm8DF"></div>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <button
                            type="submit"
                            class="btn btn-green btn-round">
                            Send
                        </button>
                        <button class="btn btn-link grey" type="reset">Clear all</button>
                    </div>
                    <input type="hidden" name="mode" value="contactus">
                </form>
            </div>
        </div>
    </div>
</section>