<?php get_header(); ?>
<?php wp_reset_postdata(); ?>
<?php $allCate = array(); ?>
<?php foreach(get_the_category() as $cate): ?>
	<?php $allCate[] = strtolower($cate->name) ?>
<?php endforeach; ?>
<?php $allCate = implode(', ',  $allCate); ?>
<?php if (preg_match("/gallery/", $allCate)): ?>
	<?php get_template_part('single-gallery', 'singleGallery'); ?>
<?php else: ?>
	<?php get_template_part('single-default', 'singleDefault'); ?>
<?php endif; ?>
<?php get_footer(); ?>