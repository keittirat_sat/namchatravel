<?php get_header(); ?>
<section class="bg_mountain">
	<div class="container">
		<?php wp_reset_postdata(); ?>
		<div class="row">
			<div class="col-xs-12">
				<h1 class="green shadow text-center page-title"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row">
			<?php $thumb = get_all_size_image(get_post_thumbnail_id()); ?>

			<?php if (!empty($thumb['thumbnail'])) : ?>
				<div class='col-sm-4'>
					<div class='thumbnail'>
						<img src="<?php echo $thumb['thumbnail']; ?>" class="img-responsive">
					</div>
				</div>
				<div class='col-sm-8'>
					<div class="wp-content">
						<?php the_content(); ?>
					</div>
				</div>
			<?php else : ?>
				<div class="col-sm-12">
					<div class="wp-content">
						<?php the_content(); ?>
					</div>
				</div>
			<?php endif; ?>

		</div>
	</div>
</section>
<?php get_footer(); ?>