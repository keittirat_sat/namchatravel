<?php get_header(); ?>
<section class="bg_mountain">
	<div class="container">
		<?php wp_reset_postdata(); ?>
		<div class="row">
			<div class="col-xs-12">
				<h1 class="green shadow text-center page-title"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="wp-content">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>