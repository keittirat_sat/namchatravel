<?php get_header(); ?>
<section class="bg_mountain" style="min-height: 100vh;">
	<div class="container">
		<?php wp_reset_postdata(); ?>
		<div class="row">
			<div class="col-xs-12">
				<h1 class="green shadow text-center page-title"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<?php $thumb = get_all_size_image(get_post_thumbnail_id()); ?>
				<img src="<?php echo $thumb['large']; ?>" class="img-responsive">
			</div>
			<div class="col-sm-8">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>