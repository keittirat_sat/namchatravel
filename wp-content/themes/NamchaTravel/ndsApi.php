<?php
$GLOBALS['requestStatus'] = null;
$GLOBALS['fbLink'] = 'http://www.facebook.com/namchatravel';
$GLOBALS['instagramLink'] = 'https://www.instagram.com/namchatravel/';
if ($_POST) {
    $siteReferer = $_SERVER['HTTP_REFERER'];
    $pd = $_POST;

    /**
     * Check Referer
     */
    if (preg_match("/namchatravel/", $siteReferer) && 
        isset($pd['mode']) && 
        isset($pd['g-recaptcha-response']) && 
        verify_capcha($pd['g-recaptcha-response'])) {

        if ($pd['mode'] == 'contactus') {
            if ($pd['contact-name'] != '') {
                $GLOBALS['requestStatus'] = true;
            } else if (!empty($pd['contact-lastname']) && !empty($pd['contact-email']) && !empty($pd['contact-subject']) && !empty($pd['contact-detail'])) {
                $contactSubject = '[ContactForm:NamchaTravel] ' . $pd['contact-subject'];
                $contactDetail = '<h3>NamchaTravel.com: Contact Form</h3>';
                $contactDetail .= '<p>Form: ' . $pd['contact-lastname'] . ' (' . $pd['contact-email'] . ')</p>';
                $contactDetail .= '<h3>Detail</h3>';
                $contactDetail .= '<p>' . nl2br($pd['contact-detail']) . '</p>';
                $contactDetail .= '<p>------------<br/>This is contact information, in case of no response please contact us by email directly</p>';

                if (nds_send_email_default($pd['contact-email'], $pd['contact-lastname'], $contactSubject, $contactDetail)) {
                    $GLOBALS['requestStatus'] = true;
                } else {
                    $GLOBALS['requestStatus'] = false;
                }
            }
        } else if ($pd['mode'] == 'testimonial') {
            if ($pd['guest-name'] != '') {
                $GLOBALS['requestStatus'] = true;
            } else if (!empty($pd['guest-lastname']) && !empty($pd['guest-email']) && !empty($pd['guest-detail'])) {
                $testimonial_name = $pd['guest-lastname'];
                $testimonial_email = $pd['guest-email'];
                $testimonial_messages = nl2br($pd['guest-detail']);

                if (insert_testimonial($testimonial_name, $testimonial_email, $testimonial_messages)) {
                    $subject .= "[Testimonial:NamchaTravel] {$testimonial_name} has created testimonial";
                    $html = "New Testimonial has creadted, please check information down here <br><br>";
                    $html .= "<b>{$testimonial_name} ({$testimonial_email})</b><br>";
                    $html .= $testimonial_messages . "<br><br>";
                    $html .= "Please approve a comment at admin system by using this link " . admin_url('admin.php?page=nds-testimonial');
                    nds_send_email_default($testimonial_email, $testimonial_name, $subject, $html);
                    $GLOBALS['requestStatus'] = true;
                } else {
                    $GLOBALS['requestStatus'] = false;
                }
            }
        }
    }
} 