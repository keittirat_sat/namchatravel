<?php get_header(); ?>
<section class="bg_mountain">
	<div class="container">
		<?php wp_reset_postdata(); ?>
		<div class="row">
			<div class="col-xs-12">
				<h1 class="green text-center page-title shadow" style="margin-bottom: 0px; padding-bottom: 0px;">Gallery</h1>
				<h4 class="text-center page-title grey" style="margin-top: 0px;"><?php the_title(); ?></h4>
			</div>
		</div>
        <div class="row" style="margin-bottom: 15px;">
            <div class="col-xs-12">
                <a href="<?php echo get_category_link(4) ?>" class="btn btn-round btn-green-inv">&lt;&nbsp;back to Gallery</a>
            </div>
        </div>
		<div class="row">
			<?php $allImg = get_all_post_image($post->ID); ?>
			<?php foreach($allImg as $img): ?>
				<div class="col-sm-3" key="<?php echo $img['img_id']; ?>">
					<a href="<?php echo $img['large']; ?>" rel='colorbox' class='thumbnail'>
						<img src="<?php echo $img['thumbnail']; ?>" class='img-responsive'>
					</a>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>