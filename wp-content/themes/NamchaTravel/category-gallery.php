<?php get_header(); ?>
<?php wp_reset_postdata(); ?>
<section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="green shadow text-center page-title"><?php wp_title(null); ?></h1>
			</div>
		</div>
		<div class="row">
			<?php foreach($posts as $post): setup_postdata($post); ?>
				<div class="col-sm-3">
					<a href="<?php the_permalink(); ?>" class="thumbnail" style='margin-bottom: 10px;'>
						<?php $thumb = get_all_size_image(get_post_thumbnail_id()); ?>
						<img src="<?php echo $thumb['thumbnail']; ?>" class="img-responsive">
					</a>
					<p class='clearfix' style='margin-bottom: 25px;'>
						<span class="text-limit">
							<?php the_title(); ?>
						</span>
						<span class='pull-right green'>
							<!--<i class="fa fa-file-image-o" aria-hidden="true"></i>-->
                            <img src="<?php bloginfo('template_directory'); ?>/img/gallery_photo_icon.png" alt="" />
							&nbsp;
							<?php $total = get_all_post_image($post->ID); ?>
							<?php echo count($total); ?>
						</span>
					</p>
				</div>
				<?php wp_reset_postdata(); ?>
			<?php endforeach; ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>