<?php get_header(); ?>
<section id="home_slide">
    <?php
    if (function_exists('meteor_slideshow')) {
        meteor_slideshow();
    }
    ?>
</section>

<section id="home_aboutus">
    <div class="container">
        <div class="row">
            <div class="col-sm-7">
                <?php $about_us_info = get_post(20); ?>
                <?php $post          = setup_postdata($about_us_info); ?>
                <div class="padding_space_xs">
                    <?php the_content(); ?>
                    <p class="text-center">
                        <a class="btn btn-round btn-white">more about us</a>
                    </p>
                </div>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</section>

<section id="home_gallery">
    <div class="container">
        <h1 class="text-center green shadow" style="margin: 25px 0 15px;">Recent photos</h1>
        <?php $all_photos    = get_all_image_from_category(4); ?>
        <?php shuffle($all_photos); ?>
        <?php $all_photos    = array_splice($all_photos, 0, 8); ?>
        <div class="row">
            <?php foreach ($all_photos as $photo): ?>
                <div class="col-sm-3">
                    <a href="<?php echo $photo['large']; ?>" class="thumbnail" rel='colorbox'>
                        <img src="<?php echo $photo['thumbnail']; ?>" class="img-responsive">
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
        <p class="text-center" style="margin-bottom: 25px;">
            <a href="<?php echo get_category_link(4) ?>" class="btn btn-round btn-green">view gallery</a>
        </p>
    </div>
</section>

<div class="top_overlay hidden-xs"></div>
<section id="home_testimonial" class="bg_grey">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <img src="<?php bloginfo('template_directory'); ?>/img/main_testimonial_text.png" class="img-responsive testimonial_logo">
            </div>
            <div class="col-sm-8">
                <div class="row hidden-lg hidden-md hidden-sm">
                    <?php $testimonials = get_testimonial(1, 5); ?>
                    <?php foreach ($testimonials['data'] as $key => $testimonial): ?>
                        <?php if ($key != 0): ?>
                            <hr/>
                        <?php endif; ?>
                        <div class="white">
                            <p><?php echo mb_substr($testimonial->detail, 0, 240); ?>&nbsp;[...]</p>
                            <p class="text-right">&mdash;&nbsp;<?php echo $testimonial->name ?><br><?php echo $testimonial->human_date ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="row hidden-xs">
                    <div class="col-sm-2 text-right">
                        <a href="#prev" class="testimonial_arrows" data-direction="prev">
                            <img src="<?php bloginfo('template_directory'); ?>/img/main_testimonial_navarrow_left_normal.png">
                        </a>
                    </div>
                    <div class="col-sm-8">
                        <?php $testimonials = get_testimonial(1, 5); ?>
                        <?php foreach ($testimonials['data'] as $key => $testimonial): ?>
                            <div id="<?php echo "testimonial-{$key}" ?>" class="white home_testimonial_block" <?php echo ($key > 0 ? "style='display: none;'" : "") ?>>
                                <p><?php echo mb_substr($testimonial->detail, 0, 240); ?>&nbsp;[...]</p>
                                <p class="text-right">&mdash;&nbsp;<?php echo $testimonial->name ?><br><?php echo $testimonial->human_date ?></p>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="col-sm-2">
                        <a href="#next" class="testimonial_arrows" data-direction="next">
                            <img src="<?php bloginfo('template_directory'); ?>/img/main_testimonial_navarrow_right_normal.png">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-center">
                <a href="<?php echo get_page_link(22) ?>" class="btn btn-round btn-green">view all</a>&nbsp;
                <button class="btn btn-round btn-default green" id="invokeTestimonial" data-toggle="modal" data-target="#testtimonialModal">
                    <i class="fa fa-plus" aria-hidden="true"></i> Our lovely guest ?, Write your own!
                </button>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            var index_testimonial = 0;
            var total_testimonial = $('.home_testimonial_block').length;
            $('.testimonial_arrows').click(function (e) {
                e.preventDefault();
                var temp = null;
                var direction = $(this).data('direction');
                if (direction == 'prev') {
                    if (index_testimonial > 0) {
                        temp = index_testimonial;
                        index_testimonial--;
                    }
                } else if (direction == 'next') {
                    if (index_testimonial < (total_testimonial - 1)) {
                        temp = index_testimonial;
                        index_testimonial++;
                    }
                }

                if (temp != null) {
                    $('#testimonial-' + temp).fadeOut(100, function () {
                        $('#testimonial-' + index_testimonial).fadeIn(100);
                    });
                }
            });
            
            $('#testtimonialModal').on('hidden.bs.modal', function(){
                $('#testimonialForm').trigger('reset');
            });
        });
    </script>
</section>
<div class="bottom_overlay hidden-xs"></div>

<section id="social_home">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-1">
                <p class="text-center" style="margin-bottom: 40px; margin-top: 20px;">
                    <img src="<?php bloginfo('template_directory'); ?>/img/main_social_text.png" class="img-responsive" alt="Stay connected with our social" />
                </p>
                <p class="text-center">
                    <a href="http://www.tripadvisor.com/Attraction_Review-d12425744" target="_blank">
                        <img src="<?php bloginfo('template_directory'); ?>/img/main_social_tripadvisor_banner.png" alt="Trip Advisor Banner" class="img-responsive" />
                    </a>
                </p>
            </div>
            <div class="col-sm-4">
                <div class="fb-page" data-href="https://www.facebook.com/namchatravel" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/namchatravel" class="fb-xfbml-parse-ignore">
                        <a href="https://www.facebook.com/namchatravel">Namcha Travel</a>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="home_program" class="bg_mountain">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <img id="home_program_logo" src="<?php bloginfo('template_directory'); ?>/img/main_program_text.png" class="img-responsive">
            </div>
            <div class="col-sm-8">
                <?php $posts    = get_posts(array('category' => 3, 'posts_per_page' => -1)); ?>
                <?php $programs = array(); ?>
                <?php foreach ($posts as $post): setup_postdata($post); ?>
                    <?php $temp           = array(); ?>
                    <?php $temp['title']  = get_the_title(); ?>
                    <?php $temp['detail'] = get_the_excerpt(); ?>
                    <?php $temp['link']   = get_the_permalink(); ?>
                    <?php $temp['thumb']  = get_all_size_image(get_post_thumbnail_id()); ?>
                    <?php $programs[]     = $temp; ?>
                <?php endforeach; ?>
                <?php wp_reset_postdata(); ?>

                <div class="home_program_detail">
                    <br>
                    <?php if (count($programs)): ?>
                        <?php foreach ($programs as $key => $program): ?>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="thumbnail">
                                        <img src="<?php echo $program['thumb']['thumbnail']; ?>" class="img-responsive">
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <h4 class='green shadow'><?php echo $program['title'] ?></h4>
                                    <?php echo $program['detail'] ?>
                                    <p class='text-right'>
                                        <a href="<?php echo $program['link'] ?>" class='btn btn-round btn-green'>
                                            Read more
                                        </a>
                                    </p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <h5 class="text-center green">No Programs Available</h5>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade testimonialModal" id="testtimonialModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content bg_green">
            <div class="modal-body">
                <h2 class="text-center white">Give us feedback</h2>
                <form class="form" action="<?php echo get_page_link(22) ?>" method='post' id="testimonialForm">
					<label>Write your own !</label>
					<div class="form-group guestFirstName">
                        <input type="text" class="form-control" name="guest-name" placeholder="Name" required="required" />
                    </div>
					<div class="form-group guestLastName">
                        <input type="text" class="form-control" name="guest-lastname" placeholder="Last Name" required="required" />
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="guest-email" placeholder="Email" required="required" />
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Message" name="guest-detail" rows="7"></textarea>
                    </div>
                    <div class="form-group clearfix">
                        <div class='pull-right'>
                            <div class="g-recaptcha" data-sitekey="6LcLKB0UAAAAAEbvWjvAPqMzKsnhKsJ3IAEDm8DF"></div>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <button
                            type="submit"
                            class="btn btn-transparent btn-round white">
                            Send feedback
                        </button>
                        <button class="btn btn-link white" type="reset" data-dismiss="modal">Cancel</button>
                    </div>
                    <input type="hidden" name="mode" value='testimonial'>
				</form>
            </div>
        </div>
    </div>
</div>

<?php get_template_part('section-contactus', 'contactus'); ?>
<?php get_footer(); ?>