<script type="text/javascript" src="<?php echo plugins_url("nds-testimonial/js/bootstrap.min.js") ?>"></script>
<link rel="stylesheet" href="<?php echo plugins_url("nds-testimonial/css/bootstrap.min.css") ?>" />
<link rel="stylesheet" href="<?php echo plugins_url("nds-testimonial/css/animate.css") ?>" />

<div class="bootstrap-scope">
    <h1>NDS Testimonial</h1>

    <hr>

    <div class="container">
        <div class="row clearfix">
            <div class="col-md-8 col-md-offset-2">
                <?php if (!empty($nds_comments)) : ?>
                    <?php foreach ($nds_comments as $each_nds_comment): ?>
                        <div class="panel <?php echo ($each_nds_comment->status == "new") ? "panel-warning" : (($each_nds_comment->status == "approved") ? "panel-success" : "panel-danger") ?>">
                            <div class="panel-heading">
                                <?php echo $each_nds_comment->name; ?> | <i><?php echo "{$each_nds_comment->email} @{$each_nds_comment->created_at}"; ?></i>
                            </div>
                            <div class="panel-body">
                                <div class="row clearfix">
                                    <?php if (isset($each_nds_comment->img_path)): ?>
                                        <div class="col-sm-4">
                                            <img src="<?php echo site_url($each_nds_comment->img_path); ?>" alt="" class="img-responsive" />
                                        </div>
                                        <div class="col-sm-8">
                                            <p><?php echo nl2br($each_nds_comment->detail); ?></p>
                                        </div>
                                    <?php else: ?>
                                        <div class="col-xs-12">
                                            <p><?php echo nl2br($each_nds_comment->detail); ?></p>
                                        </div>
                                    <?php endif; ?>
                                </div>

                                <?php if ($each_nds_comment->status == "new"): ?>
                                    <p class="text-right" style="margin: 0px; padding-top: 15px;">
                                        <a href="<?php echo "?page=nds-testimonial&action=operate&status=approved&id={$each_nds_comment->id}"; ?>" class="btn btn-operate btn-success btn-xs">Approve</a>
                                        <a href="<?php echo "?page=nds-testimonial&action=operate&status=rejected&id={$each_nds_comment->id}"; ?>" class="btn btn-operate btn-danger btn-xs">Reject</a>
                                    </p>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <h4 class="text-center">Empty List</h4>
                <?php endif; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <nav>
                    <ul class="pagination pagination-sm">
                        <?php for ($i = 0; $i < $nds_total_page; $i++): ?>
                            <li <?php echo $i == $nds_page ? "class='active'" : ""; ?>><a href="?page=nds-testimonial&nds_page=<?php echo $i; ?>"><?php echo $i + 1; ?></a></li>
                        <?php endfor; ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
//    jQuery(function () {
//        jQuery('a.btn-operate').click(function(e){
//            e.preventDefault();
//            var url = jQuery(this).attr("href");
//            jQuery.get(url, function(res){
//                console.log(res);
//            }, 'json');
//        });
//    });
</script>