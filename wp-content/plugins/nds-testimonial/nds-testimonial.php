<?php

/*
  Plugin Name: NDS-Testimonial
  Plugin URI: http://trycatch.in.th/
  Description: Testimonial API by Northerwind Digital Solution
  Version: 1.2.5
  Author: Kiattirat Sujjapongse
  Author URI: http://trycatch.in.th/
 */
global $testimonial_db_version;
$testimonial_db_version = '1.0';

function nds_testimonial_install() {
    global $wpdb;
    global $testimonial_db_version;

    $table_name = $wpdb->prefix . 'nds_testimonial';

    $create_sql = "CREATE TABLE IF NOT EXISTS `{$table_name}` (
        `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
        `name` VARCHAR(255) NULL COMMENT '',
        `email` VARCHAR(255) NULL COMMENT '',
        `img_path` TEXT NULL COMMENT '',
        `detail` TEXT NULL COMMENT '',
        `status` ENUM('new', 'rejected', 'approved') NULL DEFAULT 'new' COMMENT '',
        `created_at` TIMESTAMP NULL COMMENT '',
        `updated_at` TIMESTAMP NULL COMMENT '',
    PRIMARY KEY (`id`)  COMMENT '')
    ENGINE = MyISAM;";
    $upload_dir = WP_CONTENT_DIR . '/testimonial';
    if (!file_exists($upload_dir)) {
        mkdir($upload_dir);
        file_put_contents($upload_dir . '/index.html', 'testimonial');
    }
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta($create_sql);

    add_option('testimonial_db_version', $testimonial_db_version);
}

function nds_testimonial_init() {
    add_menu_page('NDS Testimonial', 'Testimonial', 0, 'nds-testimonial', 'nds_testimonial_main_page', 'dashicons-heart');
}

function nds_testimonial_main_page() {
    global $wpdb;
    $table_name = $wpdb->prefix . "nds_testimonial";
    $action = $_GET['action'];

    $nds_page = $_GET['nds_page'];
    $nds_page = empty($nds_page) ? 0 : $nds_page;
    $limit = 30;
    $offset = $limit * $nds_page;
    $page_action = NULL;
    switch ($action) {
        case 'operate':
            $page_action = NULL;
            $status = $_GET['status'];
            $id = $_GET['id'];
            $wpdb->update($table_name, array('status' => $status), array('id' => $id));
            break;
        default:
            break;
    }


    /**
     * Default Page
     */
    $nds_total_page = $wpdb->get_results("select count('id') as total from `{$table_name}`", OBJECT);
    $nds_total_page = ceil($nds_total_page[0]->total / $limit);
    $nds_comments = $wpdb->get_results("select * from `{$table_name}` order by `id` desc limit {$limit} offset {$offset}", OBJECT);
    $page_action = "page-main.php";

    if ($page_action != NULL) {
        include_once $page_action;
    }
}

/**
 * 
 * @global type $wpdb
 * @param int $page
 * @param int $limit
 * @param string $order     desc|asc
 * @param string $status    new|approved|rejected
 * @return type
 */
function get_testimonial($page, $limit, $order = 'desc', $status = "approved") {
    global $wpdb;
    $offset = ($page - 1) * $limit;
    $table_name = $wpdb->prefix . "nds_testimonial";
    $resp = array();
    $total_page = $wpdb->get_results("select count(id) as total from `{$table_name}` where status = '{$status}'", OBJECT);
    $resp['total_page'] = ceil($total_page[0]->total / $limit);
    $resp['data'] = $wpdb->get_results("select * from `{$table_name}` where status = '{$status}' order by `id` {$order} limit {$limit} offset {$offset}", OBJECT);
    foreach ($resp['data'] as &$each_comment) {
        $each_comment->human_date = date("D, j F Y H:i", strtotime($each_comment->created_at));
    }
    return $resp;
}

/**
 * insert_testimonial($name, $email, $detail)
 * @param type $name
 * @param type $email
 * @param type $detail
 * @param type $img_path
 */
function insert_testimonial($name, $email, $detail, $img_path = null) {
    global $wpdb;
    $table_name = $wpdb->prefix . "nds_testimonial";
    $data = array(
        "name" => $name,
        "email" => $email,
        "detail" => $detail,
        "img_path" => $img_path,
        "status" => "new",
        "created_at" => date("Y-m-d H:i:s"),
        "updated_at" => date("Y-m-d H:i:s")
    );
    return $wpdb->insert($table_name, $data);
}

register_activation_hook(__FILE__, 'nds_testimonial_install');
add_action('admin_menu', 'nds_testimonial_init');
