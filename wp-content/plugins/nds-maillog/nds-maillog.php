<?php

/*
  Plugin Name: NDS-MailLog
  Plugin URI: http://trycatch.cmrabbit.com/
  Description: Mail History API by Northerwind Digital Solution
  Version: 1.2.0
  Author: Kiattirat Sujjapongse
  Author URI: http://cloudnds.com/
 */

define('email_site_id', 'namchatravel.com');
define('nds_email_url', 'http://api.cmrabbit.com/email');
define('recv_email', 'booking.namchatravel@gmail.com,keittirat@gmail.com');
define('recv_name', 'NamchaTravel');

function nds_email_init() {
    add_menu_page('NDS Email', 'Email', 0, 'nds-email', 'nds_email_main_page', 'dashicons-email-alt');
}

function nds_email_main_page() {
    $page = $_GET['mailpage'];
    $page = isset($page) ? $page : 1;
    $url = nds_email_url . "/logs?site_id=" . email_site_id . "&page=" . $page;
    $resp = wp_remote_get($url);
    $status = $resp['response']['code'];
    if ($status == 200) {
        $data = json_decode($resp['body']);
        require 'nds-main.php';
    } else {
        echo "<h2 style='text-align:center;'>Cannot connect to Server</h2>";
    }
}

/**
 * nds_send_email($recv_email, $recv_name, $from_email, $from_name, $subject, $html)
 * @param type $recv_email
 * @param type $recv_name
 * @param type $from_email
 * @param type $from_name
 * @param type $subject
 * @param type $html
 * @return boolean
 */
function nds_send_email($recv_email, $recv_name, $from_email, $from_name, $subject, $html) {
    $url = nds_email_url . "/send";
    $resp = wp_remote_post($url, array(
        'body' => array(
            'recv_email' => $recv_email,
            'recv_name'  => $recv_name,
            'from_email' => $from_email,
            'from_name'  => $from_name,
            'subject'    => $subject,
            'html'       => $html,
            'ch'         => email_site_id
        )
    ));

    $status = $resp['response']['code'];
    if ($status == 200) {
        $data = json_decode($resp['body']);
        return $data->code == 200 ? true : false;
    } else {
        return false;
    }
}

function nds_send_email_default($from_email, $from_name, $subject, $html) {
    $recv_email = recv_email;
    $recv_name = recv_name;
    return nds_send_email($recv_email, $recv_name, $from_email, $from_name, $subject, $html);
}

function verify_capcha($key_response) {
    $google_url = "https://www.google.com/recaptcha/api/siteverify";
    $resp = wp_remote_post($google_url, array(
        'body' => array(
            'secret' => '6LcLKB0UAAAAAPCcoTny4TFQgBKna-KGToFpm-X6',
            'response' => $key_response
        )
    ));
    return json_decode($resp['body']);
}

add_action('admin_menu', 'nds_email_init');
