<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo plugins_url("nds-maillog/js/bootstrap.min.js") ?>"></script>
<script type="text/javascript" src="<?php echo plugins_url("nds-maillog/js/sweetalert.min.js") ?>"></script>
<script type="text/javascript" src="<?php echo plugins_url("nds-maillog/js/jquery.form.min.js") ?>"></script>
<link rel="stylesheet" href="<?php echo plugins_url("nds-maillog/css/bootstrap.min.css") ?>" />
<link rel="stylesheet" href="<?php echo plugins_url("nds-maillog/css/sweetalert.css") ?>" />
<link rel="stylesheet" href="<?php echo plugins_url("nds-maillog/css/animate.css") ?>" />

<div class="bootstrap-scope">
    <div class="container">
        <h1>NDS Email History <small>Logging system <span class="badge animated fadeIn"><?php echo $data->result->total; ?></span></small></h1>
        <hr />
        <div class="row">
            <div class="col-xs-12">
                <table class="table animated fadeIn">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Subject</th>
                            <th>Status</th>
                            <th>More</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data->result->data as $each_record): ?>
                            <tr>
                                <td id="<?php echo "date_{$each_record->id}"; ?>"><?php echo $each_record->updated_at; ?></td>
                                <td id="<?php echo "name_{$each_record->id}"; ?>"><?php echo $each_record->from_name; ?></td>
                                <td id="<?php echo "email_{$each_record->id}"; ?>"><?php echo $each_record->from_email; ?></td>
                                <td id="<?php echo "subject_{$each_record->id}"; ?>"><?php echo $each_record->subject; ?></td>
                                <td>
                                <?php switch ($each_record->status): case 'sent': ?>
                                        <span class='label label-success'><?php echo $each_record->status ?></span>
                                        <?php break; ?>
                                    <?php case 'fail': ?>
                                    <?php case 'spam': ?>
                                        <span class='label label-danger'><?php echo $each_record->status ?></span>
                                        <?php break; ?>
                                    <?php default: ?>
                                        <span class='label label-primary'><?php echo $each_record->status ?></span>
                                        <?php break; ?>
                                <?php endswitch; ?>
                                </td>
                                <td>
                                    <button class="btn btn-primary btn-xs read-email" data-id="<?php echo $each_record->id; ?>"><i class="glyphicon glyphicon-modal-window"></i> Read</button>
                                    <textarea class="hidden" id="<?php echo "text_{$each_record->id}"; ?>"><?php echo $each_record->text; ?></textarea>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <div style="text-align: center;">
                    <nav>
                        <ul class="pagination pagination-sm">                        
                            <?php for ($i = 1; $i <= $data->result->last_page; $i++): ?>
                                <li <?php echo $i == $data->result->current_page ? "class='active'" : ""; ?>>
                                    <a href="?page=nds-email&mailpage=<?php echo $i; ?>"><?php echo $i; ?></a>
                                </li>
                            <?php endfor; ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="modal fade" id="info-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="nds-modal-title"></h4>
                </div>
                <div class="modal-body" id="nds-modal-body" style="overflow-y: auto; max-height: 350px;"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>


<script type="text/javascript">
    $(function(){
        $('.read-email').click(function(){
            var email_id = $(this).attr('data-id');
            var subject = $("#subject_"+email_id).text();
            var email = $("#email_"+email_id).text();    
            var date = $("#date_"+email_id).text();    
            var name = $("#name_"+email_id).text();    
            var text = $("#text_"+email_id).text();   
            
            var html = "<h4>Contact information</h4>\n\
                        <strong>Email</strong> "+name+" &lt;"+email+"&gt;<br />\n\
                        <strong>Date</strong> "+date+"<br />\n\
                        <strong>Subject</strong> "+subject+"<br /><hr>\n\
                        <h4>Message</h4>"+text;
            
            $('#nds-modal-title').text(subject);
            $('#nds-modal-body').html(html);
            $('#info-modal').modal();
        });
    });
</script>